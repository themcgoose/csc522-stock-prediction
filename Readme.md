# Stock Prediction

## Targeted Stocks

This is the initial subset of stocks in the Energy sector we are going to track.  The format is:

`Subdomain - TICKER - Company_Name`

* Oil/Nat'l Gas - BP - British Petrol.
* Oil/Gas - RDS.A - Royal Dutch Shell
* Oil/Gas - XOM - Exxon
* Coal - BTU - Peabody Energy
* Coal - ARCH - ArchCoal
* Nuclear - D - DominionEnergy
* Nuclear - DUK - DukeEnergy
* Solar - CSIQ - CanadianSolar
* Solar - FSLR - FirstSolar