# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from __future__ import division
from os import listdir
from os.path import isfile, join
import re
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd

tokenize = lambda doc: doc.lower().split(" ")

# Collect news article file names
directory = "news-article-sample/data/news-data/preprocessed/solar"
file_names = [f for f in listdir(directory) if isfile(join(directory, f))]

# Create dictionary of documents
the_docs = {}
for name in file_names:
    file = directory + "/" + name
    article = ""
    for line in open(file, 'r'):
        article += line
    the_docs[name] = article
    
# Use regex to truncate articles to just the body
pattern = r"body\": \"(.+)\", \"header\""
pattern = re.compile(pattern)
for name in file_names:
    result = pattern.search(the_docs[name])
    the_docs[name] = result.group(1) # Updates each document (each value of dictionary) in dict to the body

# Put each document into a document list
docs_list = []
for name in file_names:
    docs_list.append(the_docs[name])

# Use Scikit Learn to perform TF-IDF on each document
sklearn_tfidf = TfidfVectorizer(norm='l2',min_df=0, use_idf=True, smooth_idf=False, sublinear_tf=True, tokenizer=tokenize)
sklearn_representation = sklearn_tfidf.fit_transform(docs_list)
# print(sklearn_representation.toarray()[0].tolist())

# Store the entire vocabulary in a list
vocab = []
for key in sklearn_tfidf.vocabulary_.keys():
    vocab.append(key)

# Set representation as a matrix where each row is a doc and each column is the tf-idf of a term
list_of_lists = []
for i in range(0, sklearn_representation.shape[0]):
    list_of_lists.append(sklearn_representation.toarray()[i].tolist())
d = pd.DataFrame(list_of_lists)
d.columns = vocab

# Create a Pandas Excel writer for each article (row of data frame, d) using XlsxWriter as the engine
count = 0
for name in file_names:
    writer = pd.ExcelWriter(name + '.xlsx', engine='xlsxwriter')
    # Convert the dataframe to an XlsxWriter Excel object.
    d.iloc[[count]].to_excel(writer, sheet_name='Sheet1')
    # Close the Pandas Excel writer and output the Excel file.
    writer.save()
    count = count + 1
    print('Data was written to file!')