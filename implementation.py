# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 17:15:53 2018

@author: natha
"""

import numpy as np
import h5py
import matplotlib.pyplot as plt
from PIL import *
from scipy import *
import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.model_selection import train_test_split
import nn_functions as nn
from sklearn.preprocessing import StandardScaler

plt.rcParams['figure.figsize'] = (5.0, 4.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

# Load data
stock_data = pd.read_csv("dataset/finalSet.csv")

## Reassign class labels: BUY = 1, SELL = 0
for i in range(len(stock_data)):
    if stock_data['classLabel'][i] == 'BUY':
        stock_data['classLabel'].set_value(i, 1)
    else:
        stock_data['classLabel'].set_value(i, 0)
classes = stock_data.loc[:, 'classLabel']

# Clean out NaN
working_set = stock_data.drop(['id', 'date', 'todayGrowth', 'ticker', 'classLabel'], axis=1)
imp = Imputer(missing_values='NaN', strategy='mean', axis=1)
working_set = imp.fit_transform(working_set)

# Separate our train and test sets
features = working_set
train, test, ytrain, ytest = train_test_split(features, classes)
ytrain = ytrain.astype('int')
ytest = ytest.astype('int')
ytrain = ytrain.values
ytest = ytest.values

# Scale the data
scaler = StandardScaler()
scaler.fit(train)
train = scaler.transform(train)
test = scaler.transform(test)

# Transpose training and test sets
train = train.T
test = test.T

# Initialize the network's parameters
dimensions = [21, 20, 13, 10, 9, 7, 1]

def network(x, y, dimensions, alpha = 0.05, iterations = 2000, print_cost=True):
    np.random.seed(1)
    costs = []
    
    params = nn.initialize_parameters(dimensions)
    
    for i in range(iterations):
        # Forward Propogation
        al, stores = nn.forward_model(x, params)
        
        # Cost function
        cost = nn.cost_function(al, y)
        
        # backward propogation
        gradients = nn.backward_model(al, y, stores)
        
        # update step
        params = nn.update_parameters(params, gradients, alpha)
        
        # Print cost every 100 training example
        if print_cost and i % 100 == 0:
            print("Cost after iteration %i: %f" % (i, cost))
        if print_cost and i % 100 == 0:
            costs.append(cost)
            
    # plot the cost
    plt.plot(np.squeeze(costs))
    plt.ylabel('cost')
    plt.xlabel('iterations (per tens)')
    plt.title("Learning rate =" + str(alpha))
    plt.show()
    
    return params