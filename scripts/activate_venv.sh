# if the venv already exists do nothing
if [ -d "./news_venv" ]; then
    source news_venv/bin/activate
# else create it and install requirements
else
    virtualenv news_venv
    source news_venv/bin/activate
    pip install -r requirements.txt
fi
