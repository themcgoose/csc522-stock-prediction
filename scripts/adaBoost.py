from __future__ import division

import time
import numpy as np
import matplotlib.pyplot as plt
import BGDNetWithAdjLR as nn

import pandas as pd
import sklearn.preprocessing as pp #import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Imputer
import random
import copy

plt.rcParams['figure.figsize'] = (5.0, 4.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

np.random.seed(1)
random.seed(333)

# Load data
stock_data = pd.read_csv("data/CSC522Data_Updated.csv")

## Reassign class labels: BUY = 1, SELL = 0
for i in range(len(stock_data)):
    if stock_data['classLabel'][i] == 'BUY':
        stock_data['classLabel'].set_value(i, 1)
    else:
        stock_data['classLabel'].set_value(i, 0)
classes = stock_data.loc[:, 'classLabel']
#print(classes)

# Clean out NaN
working_set = stock_data
imp = Imputer(missing_values='NaN', strategy='mean', axis=1)
working_set = imp.fit_transform(working_set)

# Separate our train and test sets
working_set = np.delete(working_set, [26], axis=1)
features = working_set
train, test, ytrain, ytest = train_test_split(features, classes)
ytrain = ytrain.astype('int')
ytest = ytest.astype('int')
ytrain = ytrain.values
ytest = ytest.values

# More preprocessing: scale the data
scaler = pp.StandardScaler()

scaler.fit(train)
train = scaler.transform(train)
test = scaler.transform(test)

# Transpose training and test sets
train = train.T
test = test.T


def createNetwork(x, y, dims, alpha = 0.05, iterations = 5000, print_cost=False):
    #print("Parameters: " + str(dims) + ", " + str(alpha) + ", " + str(iterations))
    np.random.seed(100) # For initializing weights
    costs = []
    
    weights, biases = nn.init(dims)
    
    
    for i in range(0, iterations):
        
        # Forward Propogation
        #al, stores = nn.forward_model(x, params)
        al = nn.forward_propogation(x, weights, biases)
        
        # Cost function
        cost = nn.cost_function(al, y)
        
        # backward propogation
        activation_gradients, weight_gradients, bias_gradients = nn.back_propogation(al, y)
        
        # update step
        weights, biases = nn.update(weights, biases, weight_gradients, bias_gradients, alpha)
    
    return weights, biases

def run_nn(x, weight, bias):
    costs = []
    
    # Forward Propogation to get evaluation
    al = nn.forward_propogation(x, weight, bias)
    #print((al[0]))
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    class_label = []
    
    for i in range(0, len(al[0])):
        if( al[0, i] >= 0.5 ):
            class_label.append(1)
        else:
            class_label.append(0)
    return class_label

class AdaBoost:
    
    def __init__(self, training_data_labels):
        self.training_data_labels = training_data_labels
        self.training_data_size = len(self.training_data_labels)
        # calculate initial weight for each data set
        self.weights = np.ones(self.training_data_size)/self.training_data_size
        self.class_labels_list = []
        self.alphas = []
    
    def run_iteration(self, classifed_labels):
        # calculate errors
        errors = []
        for i, class_label in enumerate(self.training_data_labels):
            errors.append(class_label!=classifed_labels[i]) # result of comparing class label between classifier and training data
        #print (errors)
        err = (errors*self.weights).sum()
        # calculate alpha
        alpha = 0.5 * np.log((1-err)/err)
        #print 'e=%.2f a=%.2f'%(e, alpha)
        
        # calculate new weights
        new_weights = np.zeros(self.training_data_size)
        for i in range(self.training_data_size):
            if errors[i] == True:
                new_weights[i] = self.weights[i] * np.exp(alpha)
            else:
                new_weights[i] = self.weights[i] * np.exp(-alpha)
        
        # do normalization
        self.weights = new_weights / new_weights.sum()
        #print (self.weights)
        # save alpha and classifier
        self.class_labels_list.append(class_labels)
        self.alphas.append(alpha)
        return self.weights
    
    def evaluate(self):
        num_of_iterations= len(self.class_labels_list)
        classified_labels = []
        for i, class_label in enumerate(self.training_data_labels):
            # convert 1 and 0 to 1 and -1
            hx = [self.alphas[iteration_index]*(1 if self.class_labels_list[iteration_index][i] == 1 else -1) for iteration_index in range(num_of_iterations)]
            if np.sign(sum(hx)) > 0 :
                classified_labels.append(1)
            else:
                classified_labels.append(0)
        TP = 0
        FP = 0
        TN = 0
        FN = 0
        for i in range(len(classified_labels)):
            if( classified_labels[i] == 1 ):
                if(self.training_data_labels[i] == 1):
                    TP = TP + 1
                else:
                    FP = FP + 1
            else:
                if(self.training_data_labels[i] == 1):
                    FN = FN + 1
                else:
                    TN = TN + 1
    
        
        accuracy = float((TP + TN))/float(len(classified_labels))
        print("Accuracy: " + str(accuracy))

    def evaluate_test_data(self, test_labels,classified_labels_list ):
        num_of_iterations = len(classified_labels_list)
        classified_labels = []
        for i, class_label in enumerate(test_labels):
            # convert 1 and 0 to 1 and -1
            hx = [self.alphas[iteration_index]*(1 if classified_labels_list[iteration_index][i] == 1 else -1) for iteration_index in range(num_of_iterations)]
            if np.sign(sum(hx)) > 0 :
                classified_labels.append(1)
            else:
                classified_labels.append(0)
        TP = 0
        FP = 0
        TN = 0
        FN = 0
        for i in range(len(classified_labels)):
            if( classified_labels[i] == 1 ):
                if(test_labels[i] == 1):
                    TP = TP + 1
                else:
                    FP = FP + 1
            else:
                if(test_labels[i] == 1):
                    FN = FN + 1
                else:
                    TN = TN + 1


        accuracy = float((TP + TN))/float(len(classified_labels))
        print("Accuracy: " + str(accuracy))



#print data, sign(class_label) == sign(sum(hx))

if __name__ == '__main__':
    
    new_training_data = np.zeros( (len(train),len(train[0])) )
    new_training_data = train.copy()
    training_labels = ytrain.copy()
    #print(new_training_data)
    m = AdaBoost(ytrain)
    nn_factors = []
    for iteration_counter in range(100):
        # train nn with data
        #print(new_training_data)
        
        # train nn with new training set
        weight, bias = createNetwork(new_training_data, training_labels, [26, 13, 13, 1], 1, 10000) # Creates the NN
        nn_factors.append((weight, bias))
        #print("weight",weight)
        #run nn with original training data
        class_labels = run_nn(train, weight, bias) # Find the accuracy
        #alternative  : pass in result of classification
        new_weights = m.run_iteration(class_labels)
        
        #####sample training data#####
        
        # genereate indexes based on weights
        data_indexs = range(len(ytrain))
        data_indexs = [np.random.choice(data_indexs, p=new_weights) for _ in range(len(ytrain))]
        data_indexs.sort()
        #print(data_indexs[30:])
        # build new training data
        for i in range(len(train)):
            attributes = np.zeros(len(train[0]))
            
            for j in range(len(data_indexs)):
                attributes[j] =  train[i][data_indexs[j]]
            new_training_data[i] = attributes
        #print(new_training_data)
        #print(training_labels)
        for i,index in enumerate(data_indexs):
            training_labels[i] = ytrain[index]
        
        print("iteration %d" %(iteration_counter))
        m.evaluate()
        test_classified_labels = []
        for (weight, bias) in nn_factors:
            class_labels = run_nn(test, weight, bias)
            test_classified_labels.append(class_labels)

        m.evaluate_test_data(ytest,test_classified_labels)

#print(training_labels)



#print(m.alphas)
#print(len(m.class_labels_list))

#        new_training_data.append(examples[index])
#    print(new_training_data)
#
#
#
#    m.run_iteration(lambda x: 2*(x[0] < 4.5)-1)
#    m.run_iteration(lambda x: 2*(x[1] > 5)-1)
#    m.evaluate()

