import json
from pprint import pprint
import datetime
OPEN = "Open"
HIGH = "High"
CLOSE = "Close"
LOW = "Low"
'''
    Usage :
    Arguements : stock name
            date of desired data
    Return values : stock went up or down
             different percentage between open and close price
             different percentage between open and high price
             different percentage between open and low price
'''
def getStockInfo(stockName,startDate,numDays):
  allData = json.load(open('%s.json' % (stockName)))
  current_date = datetime.datetime.strptime(startDate, '%Y-%m-%d')
  #print(current_date)
  #find if current day is buy or sell
  try :
      requestDate = current_date.strftime('%Y-%m-%d')
      data = allData[requestDate]
      todayGrowth = (float(data[CLOSE]) - float(data[OPEN]))/float(data[OPEN])*100
      buyOrSell =  "BUY" if todayGrowth > 0 else "SELL"
  except:
    return (0,"NA",0,0)
  #start looking into data in the past
  current_date -= datetime.timedelta(days=1)
#print(current_date)
  dayCounter = 0
  netGrowth = 0
  while dayCounter < numDays :
      #print(current_date.strftime('%Y-%m-%d'))
      #print(current_date)
    requestDate = current_date.strftime('%Y-%m-%d')
    try:
     data = allData[requestDate]
     openToClose = (float(data[CLOSE]) - float(data[OPEN]))/float(data[OPEN])*100
    
    except: # ignore date without data
     current_date -= datetime.timedelta(days=1)
     continue

#openToHigh = (float(data[HIGH]) - float(data[OPEN]))/float(data[OPEN])*100
#openToLow = (float(data[LOW]) - float(data[OPEN]))/float(data[OPEN])*100
    current_date -= datetime.timedelta(days=1)
    dayCounter += 1
    netGrowth += openToClose
#print("netGrowth %f" %(netGrowth))
#print("openToClose %f" %(openToClose))
        
  avgGrowth = netGrowth/numDays
  return (todayGrowth,buyOrSell,netGrowth,avgGrowth)
def getAllDates(stockName):
    allData = json.load(open('%s.json' % (stockName)))
    dates = list(allData.keys())
    sortedDateTime = []
    for date in dates:
        sortedDateTime.append(datetime.datetime.strptime(date, '%Y-%m-%d'))
    sortedDateTime.sort()
    del sortedDateTime[:10] # remove the first ten days
    dates = []
    for dateTime in sortedDateTime:
        dates.append(dateTime.strftime('%Y-%m-%d'))
    return dates
if __name__ == '__main__':
    stockNames = ["ARCH"]
    #
#    file_object  = open("financial_data.csv", "w")
#    file_object.write("stock,date,netGrowth1,netGrowth5,netGrowth10,avgGrowth1,avgGrowth5,avgGrowth10,classLabel\n")
#    file_object.close()

    for stockName in stockNames:
        file_object  = open("%s.csv"%(stockName), "w")
        file_object.write("stock,date,todayGrowth,netGrowth1,netGrowth5,netGrowth10,avgGrowth1,avgGrowth5,avgGrowth10,classLabel\n")
        file_object.close()
        dates = getAllDates(stockName)
        for date in dates:
            #print(date)
                #try:
            file_object  = open("%s.csv"%(stockName), "a")
            (todayGrowth,classLabel,netGrowth1,avgGrowth1) = getStockInfo(stockName,date,1)
            (_,_,netGrowth5,avgGrowth5) = getStockInfo(stockName,date,5)
            (_,_,netGrowth10,avgGrowth10) = getStockInfo(stockName,date,10)
            file_object.write("%s,%s,%f,%f,%f,%f,%f,%f,%f,%s\n" %(stockName,date,todayGrowth,netGrowth1,netGrowth5,netGrowth10,avgGrowth1,avgGrowth5,avgGrowth10,classLabel))
            file_object.close()
                #except: # ignore date without data

