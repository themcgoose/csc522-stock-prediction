Neural Network Scripts and Testing

Ensure you have the entire repository pulled if possible. Otherwise, the dataset may not be in the correct directory with respect to the other scripts. 
You'll see there are three separate Python3 files in this folder. BGDNet trains a network with Batch Gradient Descent. BGDNetWithAdjLR trains the network with Batch Gradient Descent with a dynamic learning rate. SGDNeuralNetWithMomentum trains the network with Stochastic Gradient Descent and momentum.

All three networks are trained and tested the same way as follows:
1. Open your Python file of choice
2. Run the entire file --- this will load in the data, preprocess it, and separate it into training and testing sets
3. Run something along the lines of "weights, biases = __main__()". This will forward and backward propagate through the network to learn the model's parameters
  a) You will be prompted for hyperparameters one at a time: hidden layer sizes (not counting input size (26) and output neuron (1)), learning rate (+ momentum value if SGD), and number of iterations
  b) For BGD, a good example is: Hidden Layer Sizes -- 15 12 10 9 4 3, Alpha -- 0.01, Iterations -- 50000
  c) For BGDWithAdjLR, a good example is: Hidden Layer sizes -- 26 26 26 21 17 14 10, Alpha -- 1, Iterations -- 10000
  d) For SGDWithMomentum, you will be prompted for a beta (momentum hyperparameter often around .7 to .999) -- this approach did not have the best performance for us, as it converged far too quickly to something suboptimal. Test what you like!
4. To test your learned parameters, simply call (where where train/test is the data other than the labels, and ytrain/ytest is the corresponding labels for the data):
  a)"evalNetwork(train, ytrain, weights, biases)" for training accuracy and confusion matrix
  b)"evalNetwork(test, ytest, weights, biases)" for test accuracy and confusion matrix

Thank you!