# -*- coding: utf-8 -*-
import click
import logging
import os
import json
import re

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
logger.addHandler(handler)


def extract_date(lines, parsed_file, filename):

    # RE that matches "Oct. 4, 2016 9:25 a.m. ET" or "Updated Oct. 6, 2016
    # 2:29 p.m. ET"
    r_date = re.compile(
        "(Updated)?\s?\w{3}.?\s\d+,\s\d{4}\s\d{1,2}:\d{1,2}\s[(?:a\.m\.)(?:p\.m\.)]+\s\w\w\n")
    dates = filter(r_date.match, lines)
    date_index = None

    if len(dates) > 0:
        article_date = dates[0]
        date_index = lines.index(article_date)

        article_date = article_date.replace("Updated ", "")
        article_date = article_date.strip("\n")

        parsed_file["Date"] = article_date

    else:
        logger.info("Unable to parse date out of {}".format(filename))

    return parsed_file, date_index


def cleanup_and_join_list(content):
    stripped = [i.strip("\n") for i in content]
    joined = " ".join(stripped)

    return joined


def parse_file(lines, filename):
    parsed_file = {}

    logger.debug("File {} has {} lines".format(filename, len(lines)))

    # Handle Empty File Gracefully
    if len(lines) == 0:
        logger.warning("File {} is empty, skipping...".format(filename))
        return parsed_file

    # These are pretty consistent
    parsed_file["Categories"] = lines[0].strip("\n")
    parsed_file["Title"] = lines[1].strip("\n")

    parsed_file, date_index = extract_date(lines, parsed_file, filename)

    # AP Articles are weird
    if "Associated Press" in lines[0:4]:
        logger.debug("-- AP --")

    else:
        # parsed_file["Date"] = lines[5]

        # Find the point that comments appears, everything above is the
        # "header" and everything below is the "body"
        r = re.compile("\d+\sCOMMENTS")
        comments = filter(r.match, lines)
        comment_boundary = None
        videos_boundary = None
        if len(comments) > 0:
            this_comment = comments[0]
            comment_boundary = lines.index(this_comment)
            logger.debug("---Found 'COMMENTS' at {}".format(comment_boundary))
        else:
            logger.warning(
                "Unable to find comment_boundary, not parsing {}".format(filename))

        if "Recommended Videos\n" in lines:
            videos_boundary = lines.index("Recommended Videos\n")
            logger.debug("--Found recommended videos index")

        if comment_boundary is not None:
            header_cutoff = comment_boundary
        elif date_index is not None:
            header_cutoff = date_index
        else:
            # Make a reasonable cutoff
            header_cutoff = 6

        parsed_file["header"] = lines[:header_cutoff]

        if videos_boundary is not None:
            body_cutoff = videos_boundary
        else:
            # Include till end of document
            body_cutoff = -1

        parsed_file["body"] = lines[header_cutoff + 1:body_cutoff]

        parsed_file["header"] = cleanup_and_join_list(parsed_file["header"])
        parsed_file["body"] = cleanup_and_join_list(parsed_file["body"])

    return parsed_file


@click.command()
@click.option("--input_dir", help="relative path to directory that contains files to preprocess")
@click.option("--output_dir", help="relative path to directory that preprocessed files will be written to")
@click.option("--debug", help="Enable debug output", is_flag=True)
@click.option("--limit", help="Limit operations for testing")
def main(input_dir=None, output_dir=None, debug=None, limit=None):

    if debug:
        logger.setLevel(logging.DEBUG)
        logger.debug("Debug Mode Enabled")
    else:
        logger.setLevel(logging.INFO)

    logger.info("Starting preprocessing of: {}, will write to: {}".format(
        input_dir, output_dir))

    counter = 0
    for filename in os.listdir(input_dir):
        logger.debug("Starting processing of {}".format(filename))
        with open(os.path.join(input_dir, filename), "r") as fp:
            lines = fp.readlines()

        parsed_file = parse_file(lines, filename)

        clean_fn = filename.replace(" ", "") + ".json"

        with open(os.path.join(output_dir, clean_fn), "w+") as fp:
            json.dump(parsed_file, fp, sort_keys=True)

        counter += 1
        if limit:
            if counter >= int(limit):
                break

    logger.info("Wrote {} files to {}".format(counter, output_dir))


if __name__ == '__main__':
    main()
