import sys, os
#Run on raw data to get the article dates
cwd = os.getcwd()
test_dir = cwd + '\\test'

months = ['january','february','march','april','may','june','july','august','september','october','november','december']
files = os.listdir(test_dir)
resultsf = open(cwd + '\\getDatesFromRaw.csv','w+')

for file in files:
	filepath = test_dir + '\\' + file
	rf = open(filepath, 'r')
	lines = rf.readlines()
	ds = ''
	for line in lines:
		tmpline = line.replace('.','').replace(',','').lower()
		sl = tmpline.split()
		if (len(sl) >=4) and (sl[0] in 'updated') and (sl[1] in m for m in months) and ( not sl[1].isdigit() and sl[2].isdigit() and sl[3].isdigit()):
			#print sl
			ds = sl[1] + ' ' + sl[2] + ' ' + sl[3]				
			break		
		elif ((len(sl) >=3) and (sl[0] in m for m in months) and ( not sl[0].isdigit() and sl[1].isdigit() and sl[2].isdigit())):
			#print sl
			ds = sl[0] + ' ' + sl[1] + ' ' + sl[2]			
			break	
	rf.close()
	print (file +','+ds)
	resultsf.write(file +','+ds+'\n')
resultsf.close()