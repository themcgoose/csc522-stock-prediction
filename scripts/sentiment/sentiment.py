import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import names
import os, sys
import openpyxl
import numpy as np
#Runs on xlsx files
#Simple bag of words
def word_feats(words):
    return dict([(word, True) for word in words])

pos_dict = {}
neg_dict = {}
cwd = os.getcwd()
pos_dir = cwd + '\\pos_articles'
neg_dir = cwd + '\\neg_articles'
test_dir = cwd + '\\test'
#print pos_dir
#print neg_dir
#print test_dir

#Create dict of positive words as keys and the TF-IDF as the data
#files = os.listdir(pos_dir)
#print files
#for file in files:
#	file_path = pos_dir + '\\' + file
#	book = openpyxl.load_workbook(file_path)
#	sheet = book['Sheet1']
#	for i in range(2,sheet.max_column):
#		if sheet.cell(row=1,column=i).value is None:
#			break
#		if sheet.cell(row=2,column=i).value != 0L:
#			if sheet.cell(row=1,column=i).value in pos_dict:				
#				pos_dict[sheet.cell(row=1,column=i).value] = pos_dict[sheet.cell(row=1,column=i).value] + sheet.cell(row=2,column=i).value
#			else:
#				pos_dict[sheet.cell(row=1,column=i).value] = sheet.cell(row=2,column=i).value

#print pos_dict
#Create dict of negative words as keys and the TF-IDF as the data
#files = os.listdir(neg_dir)
#for file in files:
#	file_path = neg_dir + '\\' + file
#	book = openpyxl.load_workbook(file_path)
#	sheet = book['Sheet1']
#	for i in range(2,sheet.max_column):
#		if sheet.cell(row=1,column=i).value is None:
#			break
#		if sheet.cell(row=2,column=i).value != 0L:
#			if sheet.cell(row=1,column=i).value in neg_dict:				
#				neg_dict[sheet.cell(row=1,column=i).value] = neg_dict[sheet.cell(row=1,column=i).value] + sheet.cell(row=2,column=i).value
#			else:
#				neg_dict[sheet.cell(row=1,column=i).value] = sheet.cell(row=2,column=i).value

#print '\n\n\n\n'
#print neg_dict
#sys.exit()


#Look for similar words among the positive and negative dicts and use TF-IDF to remove it from one or the other. 
#for key in list(pos_dict):
#	if key in neg_dict:
#		#print key
#		if pos_dict[key] == neg_dict[key]:
#			print "popping from both"
#			neg_dict.pop(key, None)
#			pos_dict.pop(key, None)
#		elif pos_dict[key] > neg_dict[key]:
#			neg_dict.pop(key, None)
#		else:
#			pos_dict.pop(key, None)
#print "done parsing similar words"
#sys.exit()

#Place the words in the dict into word lists since I don't feel like completely changing the structure of this code. 
positive_vocab = []
negative_vocab = []
pos_tmp = []
neg_tmp = []
#neutral_vocab = []
#for key in list(pos_dict):
#	positive_vocab.append(key)
#for key in list(neg_dict):
#	negative_vocab.append(key)

posfile = cwd + '\\pos_words.txt'
pf = open(posfile, 'r')
line = pf.readline()
#print ('adding positive words')
while line:
	if (' ' not in line) and ('!' not in line):
		pos_tmp.append(line.rstrip('\n').replace('-',''))
	line = pf.readline()
pf.close()
negfile = cwd + '\\neg_words.txt'
nf = open(negfile, 'r')
line = nf.readline()
#print ('adding negative words')
while line:
	if (' ' not in line) and ('!' not in line):
		neg_tmp.append(line.rstrip('\n').replace('-',''))
	line = nf.readline()
nf.close()
#print positive_vocab
#print negative_vocab
#sys.exit()
print len(pos_tmp)
print len(neg_tmp)
pos_vocab = np.random.choice(pos_tmp, 5000, replace=False)
neg_vocab = np.random.choice(neg_tmp, 4500, replace=False)
#print len(positive_vocab)
#print len(negative_vocab)

#create feature list of the words
pos_feat = [(word_feats(pos), 'pos') for pos in pos_vocab]
neg_feat = [(word_feats(neg), 'neg') for neg in neg_vocab]
#print pos_feat
#put together training set
train_set = neg_feat + pos_feat
 #train classifier
 
classifier = NaiveBayesClassifier.train(train_set) 
 
# Predict
#Grab words from test words and place them in a list to be processed
files = os.listdir(test_dir)
resultsf = open(cwd + '\\results.txt','w+')
#bowf = open(cwd + '\\words.txt','w+')
for file in files:
	#print file + ':'
	lfile = file.lower()
	etype = ' '	
	words = []
	neg = 0
	pos = 0
	nue = 0
	cSearch = ''
	sCompany = ' '
	file_path = test_dir + '\\' + file
	book = openpyxl.load_workbook(file_path)
	sheet = book['Sheet1']
	if (sheet.cell(row=4,column=1).value is not None) and (sheet.cell(row=4,column=2).value is not None):
		date = sheet.cell(row=4,column=1).value.strip().rstrip('\n').replace('.','') + sheet.cell(row=4,column=2).value.rstrip('\n').replace('.','')
	elif (sheet.cell(row=4,column=1).value is not None) and (sheet.cell(row=4,column=2).value is None):
		date = sheet.cell(row=4,column=1).value.strip().rstrip('\n').replace('.','')
	else: 
		date = 'jan 1'
	sdate = date.split(" ")
	if len(sdate) >=3:
		if not sdate[0].isdigit() and sdate[1].isdigit() and sdate[2].isdigit():
			true_date = sdate[0] + ' ' +sdate[1]+ ' '+sdate[2]
		elif not sdate[-3].isdigit() and sdate[-2].isdigit() and sdate[-1].isdigit():
			true_date = sdate[-3] + ' ' +sdate[-2]+ ' '+sdate[-1]
		else:
			true_date = ' '
	else:
		true_date = ' '
	#print true_date
	for i in range(2,sheet.max_column):
		if sheet.cell(row=1,column=i).value is None:
			break
		if float(sheet.cell(row=2,column=i).value) != 0.0:
			words.append(sheet.cell(row=1,column=i).value)
			cSearch = cSearch + sheet.cell(row=1,column=i).value + ' '
	
	cSearch = cSearch + lfile
	#grabbing energy sectors that are associated with each stock
	if 'gas' in cSearch: 
		etype = etype + 'gas '
	if 'oil' in cSearch:
		etype = etype + 'oil '
	if 'wind' in cSearch:
		etype = etype + 'wind '
	if 'solar' in cSearch:
		etype = etype + 'solar '
	if 'coal' in cSearch:
		etype = etype + 'coal '
	if 'nuclear' in cSearch:
		etype = etype + 'nuclear '
		
	#grabbing companies mentioned in each article
	if (' bp ' in cSearch) or (' bps ' in cSearch) or (' british ' in cSearch and ' petrol ' in cSearch): 
		sCompany = sCompany + 'bp '
	if (' rdsa ' in cSearch) or (' rdsas ' in cSearch) or (' royal ' in cSearch and ' dutch ' in cSearch and ' shell ' in cSearch): 
		sCompany = sCompany + 'rds.a '
	if (' xom ' in cSearch) or (' xoms ' in cSearch) or (' exxon ' in cSearch): 
		sCompany = sCompany + 'xom '
	if (' btu ' in cSearch) or (' btus ' in cSearch) or (' peabody ' in cSearch and ' energy ' in cSearch): 
		sCompany = sCompany + 'btu '
	if (' arch ' in cSearch) or (' archs ' in cSearch) or (' arch ' in cSearch and ' coal ' in cSearch): 
		sCompany = sCompany + 'arch '
	if (' d ' in cSearch) or (' ds ' in cSearch) or (' dominion ' in cSearch and ' energy ' in cSearch): 
		sCompany = sCompany + 'd '
	if (' duk ' in cSearch) or (' duks ' in cSearch) or (' duke ' in cSearch and ' energy ' in cSearch): 
		sCompany = sCompany + 'duk '
	if (' csiq ' in cSearch) or (' csiqs ' in cSearch) or (' canadian ' in cSearch and ' solar ' in cSearch): 
		sCompany = sCompany + 'csiq '
	if (' fslr ' in cSearch) or (' fslrs ' in cSearch) or (' first ' in cSearch and ' solar ' in cSearch): 
		sCompany = sCompany + 'fslr '
	#print cSearch
	#bowf.write(cSearch + '\n')
	for word in words:
		classResult = classifier.classify( word_feats(word))
		if classResult == 'neg':
			neg = neg + 1
		elif classResult == 'pos':
			pos = pos + 1
		else:
			nue = nue + 1
	if len(words) > 0:
		senttype = (float(pos)/len(words) - 0.50)*2
		s = 'File:' + file+ '; Date:' +true_date+ '; EnergyType:'+etype+'; Company:'+sCompany+'; Sentiment:'+str(senttype)+';\n'
		print (s)
		resultsf.write(s)
	
	
resultsf.close()
#bowf.close()
	