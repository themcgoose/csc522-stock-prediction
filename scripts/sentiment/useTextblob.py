import sys, os
from textblob import TextBlob
#Run on raw data
cwd = os.getcwd()
test_dir = cwd + '\\test'

files = os.listdir(test_dir)
resultsf = open(cwd + '\\getSfT.csv','w+')

for file in files:
	filepath = test_dir + '\\' + file
	rf = open(filepath, 'r',encoding='utf-8')
	lines = rf.readlines()
	lines = list(map(str.strip, lines))
	tmplines= " ".join(lines)
	test = TextBlob(tmplines)
	
	rf.close()
	print (file.replace(' ','') +','+str(test.sentiment.polarity))
	resultsf.write(file.replace(' ','') +','+str(test.sentiment.polarity)+'\n')
resultsf.close()