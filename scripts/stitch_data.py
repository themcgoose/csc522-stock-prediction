# Matt McNiece
# mrmcniec@ncsu.edu

# This script should
# Given an input file of the news data and stock data (future work)
# Convert those data into attributes that can be fed into a ML model.

import click
import pandas as pd
import pendulum

# tickers = ["bp", "btu", "csiq", "d", "duk", "fslr", "rds-b", "xom"]
tickers = ["ARCH", "BP", "BTU", "CSIQ", "D", "DUK", "FSLR", "RDS-B", "XOM"]
categories = ["gas", "oil", "nuclear", "wind", "coal", "solar"]

tickers_categories = {
    "ARCH": {
        "coal": 1
    },
    "BP": {
        "oil": 1,
        "gas": 1
    },
    "BTU": {
        "coal": 1
    },
    "CSIQ": {
        "solar": 1
    },
    "D": {
        "nuclear": 1,
        "coal": 1,
        "gas": 1,
        "oil": 1
    },
    "DUK": {
        "nuclear": 1,
        "coal": 1,
        "gas": 1,
        "oil": 1
    },
    "FSLR": {
        "solar": 1
    },
    "RDS-B": {
        "oil": 1,
        "gas": 1,
        "wind": 1
    },
    "XOM": {
        "oil": 1,
        "gas": 1,
    }
}


def expand_data_for_tickers(data):
    frames = []

    for t in tickers:
        data_t = data.copy()
        data_t['ticker'] = t
        frames.append(data_t)

    new_data = pd.concat(frames)

    return new_data


def generate_news_data(day_df, datetime_str, num_articles):
    # Generate summary statistics for the news for that day (total and each category)
    mean_day = day_df["sentiment"].mean()

    cat_means = {}

    # print day_df
    # print day_df["gas"]

    for cat in categories:
        cat_mask = day_df['category'].str.contains(r".*{}.*".format(cat))
        day_cat = day_df.loc[cat_mask]
        mean_cat = day_cat["sentiment"].mean()
        cat_means[cat] = mean_cat

    df1 = {
        "date": datetime_str,
        "num_articles": num_articles,
        "sentiment_day": mean_day,
        "sentiment_day_gas": cat_means["gas"],
        "sentiment_day_oil": cat_means["oil"],
        "sentiment_day_nuclear": cat_means["nuclear"],
        "sentiment_day_wind": cat_means["wind"],
        "sentiment_day_coal": cat_means["coal"],
        "sentiment_day_solar": cat_means["solar"]
    }

    return df1


def generate_news_rolling_averages(data):
    data["sentiment_5_day"] = data.rolling(5, min_periods=1)["sentiment_day"].mean()

    for cat in categories:
        data["sentiment_5_day_{}".format(cat)] = data.rolling(5, min_periods=1)["sentiment_day_{}".format(cat)].mean()

    return data


def generate_data(df):
    global tickers
    global categories

    start_date = pendulum.Pendulum(2000, 02, 20)
    end_date = pendulum.Pendulum(2018, 02, 20)
    period = pendulum.period(start_date, end_date)

    list_of_dicts = []

    print "Creating Initial Dataframe . . ."
    for dt in period.range('days'):
        formatted_dt = dt.to_date_string()
        day = df[(df['date'] == formatted_dt)]
        num_articles = day.shape[0]

        # Generate summary statistics for the news for that day (total and each category)
        df1 = generate_news_data(day, formatted_dt, num_articles)
        list_of_dicts.append(df1)

        # TODO similar implementation for financial data

        print "Partial update on {}".format(formatted_dt)

    data = pd.DataFrame(list_of_dicts, columns=[
        # "date_ticker",
        "date",
        "ticker",
        "num_articles",
        "gas",
        "oil",
        "nuclear",
        "wind",
        "coal",
        "solar",
        "sentiment_day",
        "sentiment_day_gas",
        "sentiment_day_oil",
        "sentiment_day_nuclear",
        "sentiment_day_wind",
        "sentiment_day_coal",
        "sentiment_day_solar"
    ])

    data['date'] = pd.to_datetime(data['date'])
    data = data.sort_values(by=["date"])
    data = data.set_index('date',
                          drop=False)  # modified to keep date as an actual attribute. Needed for merging later on.

    print "Dataframe populated with all news data, now calculating rolling averages . . ."

    data = generate_news_rolling_averages(data)

    print "Rolling avarages have been calculated.  Dropping empty rows to reduce dataset size . . ."

    print "Before filtering {} rows".format(data.shape[0])
    data = data.loc[data['num_articles'] != 0]
    print "After filtering {} rows".format(data.shape[0])

    return data


@click.command()
@click.option("--news_results_file", help="relative path to file that contains news results",
              default="../data/results.csv")
@click.option("--fin_results_file", help="relative path to file that contains financial data",
              default="../data/fin-data/financial_data/FinData.csv")
@click.option("--output_file", help="file to output data to")
def main(news_results_file=None, fin_results_file=None, output_file=None):
    global tickers

    print "Loading News Results from File {}".format(news_results_file)

    news = pd.read_csv(news_results_file)
    fins = pd.read_csv(fin_results_file)

    print "Preprocessing news articles . . . "

    # Data Cleanup #
    news["date"] = pd.to_datetime(news.loc[:, "date"])  # Convert Dates to a consistent format
    news.company = news.company.str.strip()  # Strip Whitespace from company names
    news = news.drop(columns=["filename"])  # Drop Filename

    fins["date"] = pd.to_datetime(fins.loc[:, "date"])  # Convert Dates to a consistent format
    fins.ticker = fins.ticker.str.strip()  # Strip Whitespace from company names

    # Drop rows where there isn't a timestamp because we can't do anything with them
    # https://stackoverflow.com/questions/23747451/filtering-all-rows-with-nat-in-a-column-in-dataframe-python
    news["TMP"] = news.date.values  # index is a DateTimeIndex
    news = news[news.TMP.notnull()]  # remove all NaT values
    news.drop(["TMP"], axis=1, inplace=True)  # delete TMP again

    fins["TMP"] = fins.date.values  # index is a DateTimeIndex
    fins = fins[fins.TMP.notnull()]  # remove all NaT values
    fins.drop(["TMP"], axis=1, inplace=True)  # delete TMP again

    # Filter out rows that are before the start of our data
    # https://stackoverflow.com/questions/29370057/select-dataframe-rows-between-two-dates/41802199
    mask = (news['date'] >= "2000-02-20") & (news['date'] <= "2018-02-20")
    news = news.loc[mask]
    news = news.sort_values(by="date")

    mask = (fins['date'] >= "2000-02-20") & (fins['date'] <= "2018-02-20")
    fins = fins.loc[mask]
    fins = fins.sort_values(by="date")

    print "Splitting the category column into unique columns . . ."
    # Split Category into an array to check for membership easily
    news["category"] = news["energyType"]
    news = news.drop(columns=["energyType"])
    news["gas"] = 0
    news["oil"] = 0
    news["nuclear"] = 0
    news["wind"] = 0
    news["coal"] = 0
    news["solar"] = 0

    for index in range(0, news.shape[0]):
        nextNum = index + 1
        tempVal = str(news[index:nextNum]["category"].item())
        for attribute in tempVal.split(" "):
            if attribute != "":
                news.loc[news.index[index], attribute] = 1
                # news[index:(index + 1)][attribute] = 1

    # news.to_csv("newsTest.csv")

    print "{} News Article Records".format(news.shape[0])

    # Generate Data from News Articles
    data = generate_data(news)

    print "Expanding data {} times to create one entry per ticker per day".format(len(tickers))
    data = expand_data_for_tickers(data)
    print "Now have {} entries".format(data.shape[0])

    print "Combining news and financial data together"
    result = pd.merge(data, fins, on=['date', 'ticker'])

    # Set is_x fields based on ticker
    for ticker in tickers_categories.keys():
        result.loc[result.ticker == ticker, 'gas'] = 0 if 'gas' not in tickers_categories[ticker] else 1
        result.loc[result.ticker == ticker, 'oil'] = 0 if 'oil' not in tickers_categories[ticker] else 1
        result.loc[result.ticker == ticker, 'nuclear'] = 0 if 'nuclear' not in tickers_categories[ticker] else 1
        result.loc[result.ticker == ticker, 'wind'] = 0 if 'wind' not in tickers_categories[ticker] else 1
        result.loc[result.ticker == ticker, 'coal'] = 0 if 'coal' not in tickers_categories[ticker] else 1
        result.loc[result.ticker == ticker, 'solar'] = 0 if 'solar' not in tickers_categories[ticker] else 1

    print "Printing results to CSV..."
    result.to_csv("result.csv")


if __name__ == '__main__':
    main()
