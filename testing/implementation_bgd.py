import time
import numpy as np
import matplotlib.pyplot as plt
import nn_function_bgd as nn

import pandas as pd
import sklearn.preprocessing as pp #import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Imputer
import random
import copy

plt.rcParams['figure.figsize'] = (5.0, 4.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

np.random.seed(1)
random.seed(333)

# Load data
stock_data = pd.read_csv("../data/CSC522Data_Updated.csv")
#print(stock_data.loc[0])

## Reassign class labels: BUY = 1, SELL = 0
for i in range(len(stock_data)):
    if stock_data['classLabel'][i] == 'BUY':
        stock_data['classLabel'].set_value(i, 1)
    else:
        stock_data['classLabel'].set_value(i, 0)
classes = stock_data.loc[:, 'classLabel']
#print(classes)

# Clean out NaN
working_set = stock_data
imp = Imputer(missing_values='NaN', strategy='mean', axis=1)
working_set = imp.fit_transform(working_set)

# Separate our train and test sets
working_set = np.delete(working_set, [26], axis=1)
features = working_set
train, test, ytrain, ytest = train_test_split(features, classes)
ytrain = ytrain.astype('int')
ytest = ytest.astype('int')
ytrain = ytrain.values
ytest = ytest.values

# More preprocessing: scale the data
#scaler = pp.StandardScaler()
scaler = pp.RobustScaler()

scaler.fit(train)
train = scaler.transform(train)
test = scaler.transform(test)

# Transpose training and test sets
train = train.T
test = test.T


dimsOld = [[26, 20, 13, 9, 7, 1], [26,25,24,23,22,21,20,1], [26, 21, 16, 11, 6, 1], 
[26, 23, 20, 17, 14, 11, 8, 5, 1], [26, 26, 17, 13, 11, 9, 9, 8, 1], [26, 24, 21, 21, 20, 17, 16, 7, 1]]
alpha=[ 0.1, 1 ] #0.0001, 0.001, 0.01, 0.1, 1
beta=[0]
iterations=[ 100000 ] #No longer needed, as when we go 10k, we also do 2k, and 5k. Making this less than 25000, will cause things to break.

def createNetwork(train, ytrain, dims, alpha = 0.05, iterations = 10000, beta = 0.86, print_cost=False):
    print("Parameters: " + str(dims) + ", " + str(alpha) + ", " + str(iterations))
    np.random.seed(100) # For initializing weights
    costs = []
    
    weights, biases = nn.init(dims)
    weight10000 = []
    biases10000 = []
    weight25000 = []
    biases25000 = []
    weight50000 = []
    biases50000 = []

    velocity_w = []
    velocity_b = []
    m = len(ytrain)

    #print("Weight length: " + str(len(weights)))
    # Initializing momentums
    for i in range(iterations):
        if i % 5000 == 0:
            print(i)
            if(i == 10000):
                weight10000 = copy.deepcopy(weights)
                biases10000 = copy.deepcopy(biases)
            if(i == 25000):
                weight25000 = copy.deepcopy(weights)
                biases25000 = copy.deepcopy(biases)
            if(i == 50000):
                weight50000 = copy.deepcopy(weights)
                biases50000 = copy.deepcopy(biases)
        # Forward Propogation
        # al, stores = nn.forward_model(x, params)
        al = nn.forward_propogation(train, weights, biases)
        
        # Cost function
        cost = nn.cost_function(al, ytrain)
        
        # backward propogation
        # gradients = nn.backward_model(al, y, stores)
        activation_gradients, weight_gradients, bias_gradients = nn.back_propogation(al, ytrain)
        # activation_gradients, weight_gradients, bias_gradients = dnn.L_model_backward()
        
        # update step
        # params = nn.update_parameters(params, gradients, alpha)
        weights, biases = nn.update(weights, biases, weight_gradients, bias_gradients, alpha)

        # Print cost every 100 training example
        #if j % 100 == 0:
            #costs.append(cost)
            #print("Cost after iteration %i: %f" % (j, cost))
    
    # plot the cost
    #plt.plot(costs)
    #plt.ylabel('cost')
    #plt.xlabel('iterations (per tens)')
    #plt.title("Learning rate =" + str(alpha))
    #plt.show()

    return weights, biases, weight10000, biases10000, weight25000, biases25000, weight50000, biases50000

def evalNetwork(x, y, weight, bias):
  
    # Forward Propogation to get evaluation
    al = nn.forward_propogation(x, weight, bias) # maybe?

    TP = 0
    FP = 0
    TN = 0
    FN = 0
    
    for i in range(0, len(al[0])):
        if( al[0, i] >= 0.5 ):
            if(y[i] == 1):
                TP = TP + 1
            else:
                FP = FP + 1
        else:
            if(y[i] == 1):
                FN = FN + 1
            else:
                TN = TN + 1                

    print("TP: " + str(TP))
    print("FP: " + str(FP))
    print("FN: " + str(FN))
    print("TN: " + str(TN))
    accuracy = (TP + TN)/len(al[0])
    print("Accuracy: " + str(accuracy))

    return accuracy

'''
def predict(features, labels, weights, biases):
    print("Predict")
    predictions = []
    TP = 0
    FP = 0
    FN = 0
    TN = 0
    
    probs = nn.forward_prop(features, weights, biases)
    # Predict Buy for probability 50% or higher, otherwise Sell
    print("Length: ", len(probs[0]))
    for i in range(0, len(probs[0])):
        pred = None
        if probs[0, i] >= 0.5:
            pred = 1
            predictions.append(pred)
        else:
            pred = 0
            predictions.append(pred)
        if pred == 1 and labels[i] == 1:
            TP += 1
        elif pred == 1 and labels[i] == 0:
            FP += 1
        elif pred == 0 and labels[i] == 1:
            FN += 1
        elif pred == 0 and labels[i] == 0:
            TN += 1
        else:
            print("Something went wrong?, label: " + str(labels[i]))
        
    print("Accuracy: "  + str(np.sum((predictions == labels)/features.shape[1])))
    print("TP: ", TP)
    print("FP: ", FP)
    print("FN: ", FN)
    print("TN: ", TN)
'''

'''
print("yTrain")
print(ytrain)
print("\nyTest")
print(ytest)
'''

accTotal = 0
count = 0
bestAcc = 0
bestDims = [0]
bestAlpha = 0
bestIterations = 0
results = ""
maxTrainAcc = 0

# The following, while random, has been seeded so as to always return the same results. For each value between 1 and the final number
# listed below, (not including the final number), choose and add that many layers to the NN that is being created. The number of levels
# is chosen randomly between 2 and 26 (as we do not want to end up with 1 multiple times at the end).
for i in range(1, 11):
    dims = []
    dims.append(26)
    dims.append(1)
    for o in range(0, i):
        dims.append(random.randint(2, 26))
    dims.sort(reverse=True)
    print(results)
    #print(dims)
    for j in range(0, len(alpha)):
        for k in range(0, len(iterations)):
            for l in range(0, len(beta)):
                #weight, bias = createNetwork(train, ytrain, dims, alpha[j], iterations[k])
                weight, bias, weight10000, bias10000, weight25000, bias25000, weight50000, bias50000 = createNetwork(train, ytrain, dims, alpha[j], iterations[k], beta[l])
                accuracy = evalNetwork(test, ytest, weight, bias)
                accuracy10000 = evalNetwork(test, ytest, weight10000, bias10000)
                accuracy25000 = evalNetwork(test, ytest, weight25000, bias25000)
                accuracy50000 = evalNetwork(test, ytest, weight50000, bias50000)


                trainingAccuracy = evalNetwork(train, ytrain, weight, bias)
                #predict(train, ytrain, weight, bias)
                if trainingAccuracy > maxTrainAcc:
                    maxTrainAcc = trainingAccuracy
                
                results = results + str(dims) + ", " + str(alpha[j]) + ", " + str("10000") + " = " + str(accuracy10000) + "\n"
                results = results + str(dims) + ", " + str(alpha[j]) + ", " + str("25000") + " = " + str(accuracy25000) + "\n"
                results = results + str(dims) + ", " + str(alpha[j]) + ", " + str("50000") + " = " + str(accuracy50000) + "\n"
                results = results + str(dims) + ", " + str(alpha[j]) + ", " + str(iterations[k]) + " = " + str(accuracy) + "\n"
                accTotal = accTotal + accuracy + accuracy10000 + accuracy25000 + accuracy50000
                count = count + 4 #3
                accuracy = max(accuracy50000, accuracy25000, accuracy10000, accuracy)
                if(accuracy > bestAcc):
                    print("New best Accuracy!")
                    print(str(dims[i]) + ", " + str(alpha[j]) + ", " + str("?") + " = " + str(accuracy))
                    bestAcc = accuracy
                    bestDims = dims
                    if( bestAcc == accuracy ):
                        bestIterations = 100000
                    elif( bestAcc == accuracy10000 ):
                        bestIterations = 10000
                    elif( bestAcc == accuracy50000 ):
                        bestIterations = 50000
                    else:
                        bestIterations = 25000
                    bestAlpha = alpha[j]

print("\n")
print(results)
print("Best: " + str(bestDims) + ", " + str(bestAlpha) + ", " + str(bestIterations) + " = " + str(bestAcc))
print("Average Accuracy: " + str(accTotal / count))
print("Max Training Accuracy: " + str(maxTrainAcc))

