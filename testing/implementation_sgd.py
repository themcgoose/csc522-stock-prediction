import time
import numpy as np
import matplotlib.pyplot as plt
import nn_function_sgd as nn

import pandas as pd
import sklearn.preprocessing as pp #import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Imputer
import random
import copy

plt.rcParams['figure.figsize'] = (5.0, 4.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

np.random.seed(1)
random.seed(333)

# Load data
stock_data = pd.read_csv("../data/CSC522Data_Updated.csv")

## Reassign class labels: BUY = 1, SELL = 0
for i in range(len(stock_data)):
    if stock_data['classLabel'][i] == 'BUY':
        stock_data['classLabel'].set_value(i, 1)
    else:
        stock_data['classLabel'].set_value(i, 0)
classes = stock_data.loc[:, 'classLabel']
#print(classes)

# Clean out NaN
working_set = stock_data
imp = Imputer(missing_values='NaN', strategy='mean', axis=1)
working_set = imp.fit_transform(working_set)

# Separate our train and test sets
working_set = np.delete(working_set, [26], axis=1)
features = working_set
train, test, ytrain, ytest = train_test_split(features, classes)
ytrain = ytrain.astype('int')
ytest = ytest.astype('int')
ytrain = ytrain.values
ytest = ytest.values

# More preprocessing: scale the data
#scaler = pp.StandardScaler()
scaler = pp.RobustScaler()

scaler.fit(train)
train = scaler.transform(train)
test = scaler.transform(test)

# Transpose training and test sets
train = train.T
test = test.T


dimsOld = [[26, 20, 13, 9, 7, 1], [26,25,24,23,22,21,20,1], [26, 21, 16, 11, 6, 1], 
[26, 23, 20, 17, 14, 11, 8, 5, 1], [26, 26, 17, 13, 11, 9, 9, 8, 1], [26, 24, 21, 21, 20, 17, 16, 7, 1]]
alpha=[0.1, 1] #0.0001, 0.001, 0.01,
beta=[0.8, 0.9, 0.999]
iterations=[70] #No longer needed, as when we go 10k, we also do 2k, and 5k. Making this less than 5000, will cause things to break.

def createNetwork(train, ytrain, dims, alpha = 0.05, iterations = 10000, beta = 0.86, print_cost=False):
    print("Parameters: " + str(dims) + ", " + str(alpha) + ", " + str(beta) + ", " + str(iterations))
    np.random.seed(100) # For initializing weights
    costs = []
    
    weights, biases = nn.init(dims)
    weight5 = []
    biases5 = []
    weight10 = []
    biases10 = []
    velocity_w = []
    velocity_b = []
    m = len(ytrain)

    #print("Weight length: " + str(len(weights)))
    # Initializing momentums
    for i in range(len(weights)):
        #print("Weight iteration: " + str(i))
        entries_w = np.multiply(weights[i].shape[0], weights[i].shape[1])
        entries_b = np.multiply(biases[i].shape[0], biases[i].shape[1])
        velocity_w.append(np.zeros(entries_w).reshape(weights[i].shape))
        velocity_b.append(np.zeros(entries_b).reshape(biases[i].shape))

    
    for i in range(0, iterations):
        if i % 5 == 0:
            print(i)
            if(i == 35):
                weight5 = copy.deepcopy(weights)
                biases5 = copy.deepcopy(biases)
            if(i == 50):
                weight10 = copy.deepcopy(weights)
                biases10 = copy.deepcopy(biases)
        # Perform a random permutation of train and ytrain for each epoch
        train = train.T
        assert(len(train) == len(ytrain))
        permute = np.random.permutation(len(train)) # randomly permutes indices so train and ytrain can be permuted the same way
        train = train[permute]
        ytrain = ytrain[permute]
        train = train.T
        # allows for training examples to be selected randomly for SGD
        for h in range(m):
            #print("H: " + str(h))
            # Forward Propagation
            # al, stores = nn.forward_model(x, params)
            al = nn.forward_prop(train[:, h].reshape(train.shape[0], 1), weights, biases)
            
            # Cost function
            temp_y = ytrain.reshape(ytrain.shape[0], 1)
            # temp_y = temp_y[:, h]
            # temp_y = temp_y.reshape(temp_y.shape[0], 1)
            cost = nn.cost_function(al, temp_y[h])
            
            # backward propagation
            # gradients = nn.backward_model(al, y, stores)
            activation_gradients, weight_gradients, bias_gradients = nn.back_prop(al, temp_y[h])
            # activation_gradients, weight_gradients, bias_gradients = dnn.L_model_backward()
            
            # update step
            # params = nn.update_parameters(params, gradients, alpha)
            weights, biases, velocity_w, velocity_b = nn.update_parameters(weights, biases, weight_gradients, bias_gradients, alpha, beta, velocity_w, velocity_b)
        
        # Print cost every 100 training example
        if print_cost and i % 100 == 0:
            print("Cost after iteration %i: %f" % (i, cost))
        if print_cost and i % 100 == 0:
            costs.append(cost)
            
    '''
    # plot the cost
    plt.plot(np.squeeze(costs))
    plt.ylabel('cost')
    plt.xlabel('iterations (per tens)')
    plt.title("Learning rate =" + str(alpha))
    plt.show()
    '''
    return weights, biases, weight5, biases5, weight10, biases10

def evalNetwork(x, y, weight, bias):
    print("Evaluate")
    
    # Forward Propogation to get evaluation
    al = nn.forward_prop(x, weight, bias) # maybe?

    TP = 0
    FP = 0
    TN = 0
    FN = 0
    
    for i in range(0, len(al[0])):
        if( al[0, i] >= 0.5 ):
            if(y[i] == 1):
                TP = TP + 1
            else:
                FP = FP + 1
        else:
            if(y[i] == 1):
                FN = FN + 1
            else:
                TN = TN + 1                

    print("TP: " + str(TP))
    print("FP: " + str(FP))
    print("FN: " + str(FN))
    print("TN: " + str(TN))
    accuracy = (TP + TN)/len(al[0])
    print("Accuracy: " + str(accuracy))

    return accuracy

def predict(features, labels, weights, biases):
    print("Predict")
    predictions = []
    TP = 0
    FP = 0
    FN = 0
    TN = 0
    
    probs = nn.forward_prop(features, weights, biases)
    # Predict Buy for probability 50% or higher, otherwise Sell
    print("Length: ", len(probs[0]))
    for i in range(0, len(probs[0])):
        pred = None
        if probs[0, i] >= 0.5:
            pred = 1
            predictions.append(pred)
        else:
            pred = 0
            predictions.append(pred)
        if pred == 1 and labels[i] == 1:
            TP += 1
        elif pred == 1 and labels[i] == 0:
            FP += 1
        elif pred == 0 and labels[i] == 1:
            FN += 1
        elif pred == 0 and labels[i] == 0:
            TN += 1
        else:
            print("Something went wrong?, label: " + str(labels[i]))
        
    print("Accuracy: "  + str(np.sum((predictions == labels)/features.shape[1])))
    print("TP: ", TP)
    print("FP: ", FP)
    print("FN: ", FN)
    print("TN: ", TN)
'''
print("yTrain")
print(ytrain)
print("\nyTest")
print(ytest)
'''

accTotal = 0
count = 0
bestAcc = 0
bestDims = [0]
bestAlpha = 0
bestBeta = 0
bestIterations = 0
results = ""
maxTrainAcc = 0

# The following, while random, has been seeded so as to always return the same results. For each value between 1 and the final number
# listed below, (not including the final number), choose and add that many layers to the NN that is being created. The number of levels
# is chosen randomly between 2 and 26 (as we do not want to end up with 1 multiple times at the end).
print("Beginning of main loop")
for i in range(3, 6):
    dims = []
    dims.append(26)
    dims.append(1)
    for o in range(0, i):
        dims.append(random.randint(2, 26))
    dims.sort(reverse=True)
    #print(dims)
    for j in range(0, len(alpha)):
        for k in range(0, len(iterations)):
            for l in range(0, len(beta)):
                #weight, bias = createNetwork(train, ytrain, dims, alpha[j], iterations[k])
                weight, bias, weight5, bias5, weight10, bias5000 = createNetwork(train, ytrain, dims, alpha[j], iterations[k], beta[l])
                accuracy = evalNetwork(test, ytest, weight, bias)
                #predict(test, ytest, weight, bias)
                accuracy2000 = evalNetwork(test, ytest, weight5, bias5)
                #predict(test, ytest, weight5, bias5)
                accuracy5000 = evalNetwork(test, ytest, weight10, bias5000)
                #predict(test, ytest, weight10, bias5000)

                trainingAccuracy = evalNetwork(train, ytrain, weight, bias)
                #predict(train, ytrain, weight, bias)
                if trainingAccuracy > maxTrainAcc:
                    maxTrainAcc = trainingAccuracy
                
                results = results + str(dims) + ", " + str(alpha[j]) + ", " + str(beta[l]) + ", " + str("5") + " = " + str(accuracy2000) + "\n"
                results = results + str(dims) + ", " + str(alpha[j]) + ", " + str(beta[l]) + ", " + str("10") + " = " + str(accuracy5000) + "\n"
                results = results + str(dims) + ", " + str(alpha[j]) + ", " + str(beta[l]) + ", " + str(iterations[k]) + " = " + str(accuracy) + "\n"
                accTotal = accTotal + accuracy + accuracy2000 + accuracy5000
                count = count + 3 #3
                accuracy = max(accuracy5000, accuracy2000, accuracy)
                if(accuracy > bestAcc):
                    print("New best Accuracy!")
                    print(str(dims[i]) + ", " + str(alpha[j]) + ", " + str(beta[l])+ ", " + str("?") + " = " + str(accuracy))
                    bestAcc = accuracy
                    bestDims = dims
                    bestBeta = beta[l]
                    if( bestAcc == accuracy ):
                        bestIterations = 20
                    elif( bestAcc == accuracy2000 ):
                        bestIterations = 5
                    else:
                        bestIterations = 10
                    bestAlpha = alpha[j]

print("\n")
print(results)
print("Best: " + str(bestDims) + ", " + str(bestAlpha) + ", " + str(bestBeta) + ", " + str(bestIterations) + " = " + str(bestAcc))
print("Average Accuracy: " + str(accTotal / count))
print("Max Training Accuracy: " + str(maxTrainAcc))

