# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 13:11:41 2018

@author: natha
"""

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Imputer

plt.rcParams['figure.figsize'] = (5.0, 4.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

# Global variables
glob_a = [] # the input (features) of each layer in the network
glob_w = [] # the weights corresponding to each input feature in each layer
glob_b = [] # the bias of each layer in the network
glob_z = []

glob_test = None
globular_test = None

# Load data
print("########## Reading in data ##########")
stock_data = pd.read_csv("../data/CSC522Data_Updated.csv")

## Reassign class labels: BUY = 1, SELL = 0
for i in range(len(stock_data)):
    if stock_data['classLabel'][i] == 'BUY':
        stock_data['classLabel'].set_value(i, 1)
    else:
        stock_data['classLabel'].set_value(i, 0)
    classes = stock_data.loc[:, 'classLabel']

print("########## Preprocessing data... ##########")
# Clean out NaN
working_set = stock_data.drop('classLabel', axis=1)
imp = Imputer(missing_values='NaN', strategy='mean', axis=1)
working_set = imp.fit_transform(working_set)
working_set = preprocessing.scale(working_set)

print("########## Separating into training and test sets ##########")
# Separate our train and test sets
features = working_set
train, test, ytrain, ytest = train_test_split(features, classes)
ytrain = ytrain.astype('int')
ytest = ytest.astype('int')
ytrain = ytrain.values
ytest = ytest.values

# More preprocessing
scaler = StandardScaler()
train = scaler.fit_transform(train)
test = scaler.fit_transform(test)

# Transpose training and test sets
train = train.T
test = test.T

velocity_w = []
velocity_b = []
squares_w = []
squares_b = []

the_mean = None
the_variance = None

def __main__():
    global velocity_w
    global velocity_b
    global squares_w
    global squares_b

    theIn = input("Enter size of each hidden layer that you would like separated by spaces: ")
    dims = [26] # begin with number of input features
    for i in theIn.split():
        dims.append(int(i))
    dims.append(1) # end with the output layer
    theIn = input("Enter desired learning rate: ")
    alpha = float(theIn)
    #theIn = input("Enter desired beta1 value: ")
    #beta1 = float(theIn)
    #theIn = input("Enter desired beta2 value: ")
    #beta2 = float(theIn)
    theIn = input("Enter desired number of iterations: ")
    iterations = int(theIn)
    
    weights, biases = init(dims)
    costs = []
    #for i in range(len(weights)):
        #entries_w = np.multiply(weights[i].shape[0], weights[i].shape[1])
        #entries_b = np.multiply(biases[i].shape[0], biases[i].shape[1])
        #velocity_w.append(np.zeros(entries_w).reshape(weights[i].shape))
        #velocity_b.append(np.zeros(entries_b).reshape(biases[i].shape))
        #squares_w.append(np.zeros(entries_w).reshape(weights[i].shape))
        #squares_b.append(np.zeros(entries_b).reshape(biases[i].shape))
    
    for j in range(iterations):
        # adjustable learning rate
        #if j % 1000 == 0 and j > 0:
        #    alpha /= 1.4
        
        # Forward Propogation
        al = forward_propogation(train, weights, biases)
        
        # Cost function
        cost = cost_function(al, ytrain)
        
        # backward propogation
        activation_gradients, weight_gradients, bias_gradients = back_propogation(al, ytrain)
        
        # update step
        weights, biases = update(weights, biases, weight_gradients, bias_gradients, alpha)
        
        # Print cost every 100 training example
        if j % 100 == 0:
            costs.append(cost)
            print("Cost after iteration %i: %f" % (j, cost))
            
    # plot the cost
    plt.plot(costs)
    plt.ylabel('Cost')
    plt.xlabel('Iterations (per hundreds)')
    plt.title("Neural Network with Batch Gradient Descent")
    plt.show()
    
    return weights, biases
    
    
def init(dims):
    weights = [] ## a list where indices 0, 2, ..., 2i ... are weights, and indices 1, 3, ..., 2i + 1 are biases
    biases = []
    num = len(dims)
    # Initialize random weights and biases for each layer in the network
    # Number of weight-bias-pairs corresponds to the input (dims) which is
    # a list where each entry indicates the number of nodes at each layer of the network (input, hidden_1, ..., hidden_n, output)
    # i = 0
    for i in range(num - 1):
        np.random.seed(100)
        weights.append(np.random.randn(dims[i + 1], dims[i]) / np.sqrt(num)) # random weights for dimensions
        np.random.seed(100)
        biases.append(np.zeros((dims[i + 1], 1))) # bias set to 0

    return weights, biases

# The nonlinear sigmoid activation function for output layer
def sigmoid(z):
    return 1 / (1 + np.exp(-z))

# The nonlinear relu activation function for each hidden layer (including first (input) layer)
def relu(z):
    return np.maximum(0, z)

# The derivative of the relu function for backprop
# ReLU has two distinct slopes on two distinct intervals
def relu_derivative(d_a, z):
    d_z = np.array(d_a, copy = True) # the required shape for the output
    d_z[z <= 0] = 0
    return d_z

# The derivative of the sigmoid function for backprop
def sigmoid_derivative(d_a, z):
    sig = sigmoid(z)    
    return d_a * sig * (1 - sig)

# Compute the cost
# Cross entropy cost function
def cost_function(al, y):
    m = y.shape[0] # number of examples
    e = 1 * 10 ** -7

    return (1 / m) * -(np.dot(y, np.log(al + e).T) + np.dot(1 - y, np.log(1 - al + e).T))
    #return -np.average(y * np.log(al) + (1 - y) * np.log(1 - al))

def cost_function2(al, y):
    m = y.shape[0]
    return float((1.0 / m) * np.sum((y - al) ** 2))


def forward_propogation(feat, weights, biases):
    e = 1 * 10 ** -7
    global glob_a
    global glob_w
    global glob_b
    global glob_z
    global the_mean
    global the_variance
    glob_a = []
    glob_w = []
    glob_b = []
    glob_z = []
    
    
    layers = len(weights) # how many layers in the net (discounting input layer) corresponds number of weight sets we have
    
    layers = len(weights) # how many layers in the net (discounting input layer) corresponds number of weight sets we have
    # Here are the relu layers
    a = feat
    # Here are the relu layers
    for i in range(layers-1): # access corresponding weights and biases per layer (everything but output layer)
        a_temp = a
        w = weights[i]
        b = biases[i]
        z = np.dot(w, a_temp) + b
        glob_a.append(a)
        glob_w.append(w)
        glob_b.append(b)
        a = relu(z) # feed linear combination through activation function
        glob_z.append(z)
    
    # Feed linear combination through sigmoid function for output
    w = weights[layers - 1]
    b = biases[layers - 1]
    z = np.dot(w, a) + b
    glob_a.append(a)
    glob_w.append(w)
    glob_b.append(b)
    out = sigmoid(z)
    glob_z.append(z)

        
    # print("##### Finished Forward Propogation #####")
    return out

def back_propogation(out, y):
    global glob_a
    global glob_w
    global glob_b
    global glob_z  
    e = 1 * 10 ** -7
    
    activation_gradients = []
    weight_gradients = []
    bias_gradients = []
    layers = len(glob_a)
    m = len(y)
    y = y.reshape(out.shape) # make y a numpy array
    d_out = -(np.divide(y, out + e) - np.divide(1 - y, 1 - out + e)) # partial of cost wrt activation of last layer (deeplearning.ai) 

    # activation
    # backward propogate through output layer using sigmoid derivative
    a = glob_a[layers - 1]
    w = glob_w[layers - 1]
    z = glob_z[layers - 1]
     # number of training examples
    d_z = sigmoid_derivative(d_out, z)
    d_a = np.dot(w.T, d_z)
    d_w = np.dot(d_z, a.T) / m
    d_b = np.sum(d_z, axis = 1, keepdims = True) / m

    
    activation_gradients.append(d_a)
    weight_gradients.append(d_w)
    bias_gradients.append(d_b)
    
    # This will store all of the gradients in backwards order 
    # (i.e. first entry is all the gradients of the input to the output layer)
    i = layers - 2
    while i >= 0:
        a = glob_a[i]
        w = glob_w[i]
        z = glob_z[i]
        d_z = relu_derivative(d_a, z)
        d_w = np.dot(d_z, a.T) / m
        d_b = np.sum(d_z, axis = 1, keepdims = True) / m
        d_a = np.dot(w.T, d_z)

        activation_gradients.append(d_a)
        weight_gradients.append(d_w)
        bias_gradients.append(d_b)
        
        i -= 1
        
    # print("##### Finished Back Propogation #####")
    return activation_gradients, weight_gradients, bias_gradients


def update(weights, biases, weight_gradients, bias_gradients, alpha):
    global velocity_w
    global velocity_b
    global squares_w
    global squares_b
    """
    Update params using stochastic gradient descent (momentum)

    :param weights: all of the weights
    :param biases: all of the biases
    :param grads: dict containing gradients from L backward
    :param alpha: learning rate
    :param beta1 and beta2: adam hyperparamaters
    :return: the updated parameters
    """  
    layers = len(weights)  # number of layers in the neural network
    e = 1 * 10 ** -7 # avoids division by 0
    
    # These are the lists that will accumulate contain "bias corrected" update estimates for Adam
    #bias_velocity_w = [0] * layers
    #bias_velocity_b = [0] * layers
    #bias_squares_w = [0] * layers
    #bias_squares_b = [0] * layers
    
    # velocities ordered by first to last layer
    # gradients ordered by last to first layer
    # The weight update formula
    for i in range(layers):
        # velocities
        #velocity_w[i] = beta1 * velocity_w[i] + (1 - beta1) * weight_gradients[layers - 1 - i]
        #velocity_b[i] = beta1 * velocity_b[i] + (1 - beta1) * bias_gradients[layers - 1 - i]
        #bias_velocity_w[i] = velocity_w[i] / (1 - (beta1 ** t))
        #bias_velocity_b[i] = velocity_b[i] / (1 - (beta1 ** t))
        #squares_w[i] = beta2 * squares_w[i] + (1 - beta2) * (weight_gradients[layers - 1 - i] ** 2)
        #squares_b[i] = beta2 * squares_b[i] + (1 - beta2) * (bias_gradients[layers - 1 - i] ** 2)
        #bias_squares_w[i] = squares_w[i] / (1 - (beta2 ** t))
        #bias_squares_b[i] = squares_b[i] / (1 - (beta2 ** t))
        
        # The weight update
        #weights[i] -= alpha * (bias_velocity_w[i] / (bias_squares_w[i] ** (1/2) + e))
        #biases[i] -= alpha * (bias_velocity_b[i] / (bias_squares_b[i] ** (1/2) + e))
        weights[i] -= alpha * weight_gradients[layers - i - 1]
        biases[i] -= alpha * bias_gradients[layers - i - 1]
        weights[i] = np.clip(weights[i], -1, 1)
        biases[i] = np.clip(biases[i], -1, 1)
        

    return weights, biases

# Uncomment when ready to run train network
#if __name__ == "__main__":
#    __main__()
