# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 13:11:41 2018

@author: natha
"""

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Imputer

plt.rcParams['figure.figsize'] = (5.0, 4.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

# Global variables
glob_a = [] # the input (features) of each layer in the network
glob_w = [] # the weights corresponding to each input feature in each layer
glob_b = [] # the bias of each layer in the network
glob_z = []
velocity_w = []
velocity_b = []
# cost = None

def __main__():
    global velocity_w
    global velocity_b
    # global cost
    # Load data
    print("########## Reading in data ##########")
    stock_data = pd.read_csv("../data/CSC522Data_Updated.csv")

    ## Reassign class labels: BUY = 1, SELL = 0
    for i in range(len(stock_data)):
        if stock_data['classLabel'][i] == 'BUY':
            stock_data['classLabel'].set_value(i, 1)
        else:
            stock_data['classLabel'].set_value(i, 0)
        classes = stock_data.loc[:, 'classLabel']

    print("########## Preprocessing data... ##########")
    # Clean out NaN
    working_set = stock_data.drop('classLabel', axis=1)
    imp = Imputer(missing_values='NaN', strategy='mean', axis=1)
    working_set = imp.fit_transform(working_set)
    working_set = preprocessing.scale(working_set)

    print("########## Separating into training and test sets ##########")
    # Separate our train and test sets
    features = working_set
    train, test, ytrain, ytest = train_test_split(features, classes)
    ytrain = ytrain.astype('int')
    ytest = ytest.astype('int')
    ytrain = ytrain.values
    ytest = ytest.values

    # More preprocessing
    scaler = StandardScaler()
    train = scaler.fit_transform(train)
    test = scaler.fit_transform(test)

    # Transpose training and test sets
    train = train.T
    test = test.T

    theIn = input("Enter size of each hidden layer that you would like separated by spaces: ")
    dims = [26] # begin with number of input features
    for i in theIn.split():
        dims.append(int(i))
    dims.append(1) # end with the output layer
    theIn = input("Enter desired learning rate: ")
    alpha = float(theIn)
    theIn = input("Enter desired beta value: ")
    beta = float(theIn)
    theIn = input("Enter desired number of epochs: ")
    iterations = int(theIn)
    
    costs = []
    m = len(ytrain)
    weights, biases = init(dims)
    velocity_w = []
    velocity_b = []
    # Initializing momentums
    for i in range(len(weights)):
        entries_w = np.multiply(weights[i].shape[0], weights[i].shape[1])
        entries_b = np.multiply(biases[i].shape[0], biases[i].shape[1])
        velocity_w.append(np.zeros(entries_w).reshape(weights[i].shape))
        velocity_b.append(np.zeros(entries_b).reshape(biases[i].shape))
        
    # cost = None
    for j in range(iterations):
        # Perform a random permutation of train and ytrain for each epoch
        train = train.T
        assert(len(train) == len(ytrain))
        permute = np.random.permutation(len(train)) # randomly permutes indices so train and ytrain can be permuted the same way
        train = train[permute]
        ytrain = ytrain[permute]
        train = train.T
        # allows for training examples to be selected randomly for SGD
        epoch_costs = []
        for h in range(m):
            # Forward Propagation
            # al, stores = nn.forward_model(x, params)
            al = forward_prop(train[:, h].reshape(train.shape[0], 1), weights, biases)
            
            # Cost function
            temp_y = ytrain.reshape(ytrain.shape[0], 1)
            # temp_y = temp_y[:, h]
            # temp_y = temp_y.reshape(temp_y.shape[0], 1)
            cost = cost_function(al, temp_y[h])
            epoch_costs.append(cost)
            
            # backward propagation
            # gradients = nn.backward_model(al, y, stores)
            activation_gradients, weight_gradients, bias_gradients = back_prop(al, temp_y[h])
            # activation_gradients, weight_gradients, bias_gradients = dnn.L_model_backward()
            
            # update step
            # params = nn.update_parameters(params, gradients, alpha)
            weights, biases = update_parameters(weights, biases, weight_gradients, bias_gradients, alpha, beta)
            
        # Average the costs found in the epoch
        avg = np.average(epoch_costs)
        if j % 10 == 0:
            print("Cost after epoch ", j, ": ", round(avg, 5))
            costs.append(avg)
        # reset after every epoch
        epoch_costs = []
            
    # plot the cost
    plt.plot(costs)
    plt.ylabel('cost')
    plt.xlabel('epochs (measured in 10s)')
    plt.title("Stochastic Gradient Descent Cost Behavior")
    plt.show()
    
    return weights, biases
    
'''    
def init(dims):
    weights = [] ## a list where indices 0, 2, ..., 2i ... are weights, and indices 1, 3, ..., 2i + 1 are biases
    biases = []
    num = len(dims)
    # Initialize random weights and biases for each layer in the network
    # Number of weight-bias-pairs corresponds to the input (dims) which is
    # a list where each entry indicates the number of nodes at each layer of the network (input, hidden_1, ..., hidden_n, output)
    # i = 0
    for i in range(num - 1):
        np.random.seed(100)
        weights.append(np.random.randn(dims[i + 1], dims[i]) / np.sqrt(2 / dims[i])) # He Initialization
        np.random.seed(100)
        biases.append(np.zeros((dims[i + 1], 1))) # bias set to 0

    return weights, biases
'''

def init(dims):
    weights = [] ## a list where indices 0, 2, ..., 2i ... are weights, and indices 1, 3, ..., 2i + 1 are biases
    biases = []
    num = len(dims)
    # Initialize random weights and biases for each layer in the network
    # Number of weight-bias-pairs corresponds to the input (dims) which is
    # a list where each entry indicates the number of nodes at each layer of the network (input, hidden_1, ..., hidden_n, output)
    # i = 0
    for i in range(num - 1):
        np.random.seed(100)
        weights.append(np.random.randn(dims[i + 1], dims[i]) / np.sqrt(num)) # random weights for dimensions
        np.random.seed(100)
        biases.append(np.zeros((dims[i + 1], 1))) # bias set to 0

    return weights, biases

# The nonlinear sigmoid activation function for output layer
def sigmoid(z):
    return 1 / (1 + np.exp(-z))

# The nonlinear relu activation function for each hidden layer (including first (input) layer)
def relu(z):
    return np.maximum(0, z)

# The derivative of the relu function for backprop
# ReLU has two distinct slopes on two distinct intervals
def relu_derivative(d_a, z):
    d_z = np.array(d_a, copy = True) # the required shape for the output
    d_z[z <= 0] = 0
    return d_z

# The derivative of the sigmoid function for backprop
def sigmoid_derivative(d_a, z):
    sig = sigmoid(z)    
    return d_a * sig * (1 - sig)

# Compute the cost
# Cross entropy cost function
def cost_function(al, y):
    m = y.shape[0] # number of examples

    return np.squeeze((1 / m) * -(np.dot(y, np.log(al).T) + np.dot(1 - y, np.log(1 - al).T)))


def forward_prop(feat, weights, biases):
    global glob_a
    global glob_w
    global glob_b
    global glob_z
    glob_a = []
    glob_w = []
    glob_b = []
    glob_z = []
    
    layers = len(weights) # how many layers in the net (discounting input layer) corresponds number of weight sets we have
    # Here are the relu layers
    a = feat
    for i in range(layers-1): # access corresponding weights and biases per layer (everything but output layer)
        a_temp = a
        w = weights[i]
        b = biases[i]
        
        z = np.dot(w, a_temp) + b
        glob_a.append(a)
        glob_w.append(w)
        glob_b.append(b)
        a = relu(z) # feed linear combination through activation function
        glob_z.append(z)
    
    # Feed linear combination through sigmoid function for output
    w = weights[layers - 1]
    b = biases[layers - 1]
    z = np.dot(w, a) + b
    glob_a.append(a)
    glob_w.append(w)
    glob_b.append(b)
    out = sigmoid(z)
    glob_z.append(z)

        
    # print("##### Finished Forward Propagation #####")
    return out

def back_prop(out, y):
    global glob_a
    global glob_w
    global glob_b
    global glob_z
    
    activation_gradients = []
    weight_gradients = []
    bias_gradients = []
    layers = len(glob_a)
    m = len(y)
    y = y.reshape(y.shape[0], 1)
    d_out = -(np.divide(y, out) - np.divide(1 - y, 1 - out)) # partial of cost wrt activation of last layer (deeplearning.ai)

    # activation
    # backward propagate through output layer using sigmoid derivative
    a = glob_a[layers - 1]
    w = glob_w[layers - 1]
    z = glob_z[layers - 1]
     # number of training examples
    d_z = sigmoid_derivative(d_out, z) # partial of cost wrt z
    d_a = np.dot(w.T, d_z) # partial of cost wrt a
    d_w = np.dot(d_z, a.T) / m # partial of cost wrt w
    d_b = np.sum(d_z, axis = 1, keepdims = True) / m # partial of cost wrt b
    activation_gradients.append(d_a)
    weight_gradients.append(d_w)
    bias_gradients.append(d_b)
    
    # This will store all of the gradients in backwards order 
    # (i.e. first entry is all the gradients of the input to the output layer)
    i = layers - 2
    while i >= 0:
        a = glob_a[i]
        w = glob_w[i]
        z = glob_z[i]
        d_z = relu_derivative(d_a, z)
        d_w = np.dot(d_z, a.T) / m
        d_b = np.sum(d_z, axis = 1, keepdims = True) / m
        d_a = np.dot(w.T, d_z)

        activation_gradients.append(d_a)
        weight_gradients.append(d_w)
        bias_gradients.append(d_b)
        
        i -= 1
        
    # print("##### Finished Back Propagation #####")
    return activation_gradients, weight_gradients, bias_gradients


def update_parameters(weights, biases, weight_gradients, bias_gradients, alpha, beta, velocity_w, velocity_b):
    """
    Update params using stochastic gradient descent (momentum)

    :param weights: all of the weights
    :param biases: all of the biases
    :param grads: dict containing gradients from L backward
    :param alpha: learning rate
    :param beta: momentum hyperparamaters
    :return: the updated parameters
    """  
    layers = len(weights)  # number of layers in the neural network
    e = np.exp(-8) # avoids division by 0
    
    # velocities ordered by first to last layer
    # gradients ordered by last to first layer
    # The weight update formula
    for i in range(layers):
        # velocities
        velocity_w[i] = beta * velocity_w[i] + (1 - beta) * weight_gradients[layers - 1 - i]
        velocity_b[i] = beta * velocity_b[i] + (1 - beta) * bias_gradients[layers - 1 - i]  
        # weights
        weights[i] -= alpha * (velocity_w[i] + e)
        biases[i] -= alpha * (velocity_b[i] + e) # the addition of some epsilon avoids division by 0

    return weights, biases, velocity_w, velocity_b

# Uncomment when ready to run train network
if __name__ == "__main__":
    __main__()
